#!/bin/bash

mkdir -p $DOCKER_PROJECT_PATH && cd $DOCKER_PROJECT_PATH

npm init -y
npm install react react-dom
npm install --save-dev webpack webpack-dev-server webpack-cli
npm install --save-dev @babel/core @babel/preset-env @babel/preset-react babel-loader
npm install --save-dev --save-exact prettier
npm --save-dev install eslint eslint-loader babel-eslint eslint-config-react eslint-plugin-react
npm install --save-dev less less-loader css-loader style-loader
npm install html-webpack-plugin -D

tail -f /dev/null